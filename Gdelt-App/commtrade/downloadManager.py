import time
import datetime
import logging

DEFAULT_BURST_WINDOW = datetime.timedelta(seconds=1800)
DEFAULT_WAIT_WINDOW = datetime.timedelta(seconds=100)

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
"""
Manages data download as per restrictions applied by UN Comm Trade Open API
"""

class DownloadManager:
    throttle = None
    logger = None

    def __init__(self):
        self.throttle = Throttle()

    def send(self, function, *args):
        from urllib import error
        wait_time = self.throttle.throttle()
        processed = False
        while not processed:
            while wait_time > datetime.timedelta(0):
                logging.info("Waiting for {} secs".format(wait_time.total_seconds()))
                time.sleep(wait_time.total_seconds())
                wait_time = self.throttle.throttle()

            logging.info("Executing request")
            processed = function(*args)
            if not processed:
                logging.info("Request failed, pausing for 2 minutes")
                time.sleep(120)



class Throttle(object):
    max_hits = None
    hits = None
    burst_window = None
    total_window = None
    timestamp = None

    def __init__(self, burst_window=DEFAULT_BURST_WINDOW, wait_window=DEFAULT_WAIT_WINDOW, max_hits=100):
        self.max_hits = max_hits
        self.hits = 0
        self.burst_window = burst_window
        self.total_window = burst_window + wait_window
        self.timestamp = datetime.datetime.min

    def throttle(self):
        now = datetime.datetime.utcnow()
        if now < self.timestamp + self.total_window:
            if (now < self.timestamp + self.burst_window) and (self.hits < self.max_hits):
                logging.info("Accepted - resume request {} hits".format(self.hits))
                self.hits += 1
                return datetime.timedelta(0)
            else:
                return self.timestamp + self.total_window - now
        else:
            self.timestamp = now
            self.hits = 1
            logging.info("Accepted - resume request {} hits".format(self.hits))
            return datetime.timedelta(0)
