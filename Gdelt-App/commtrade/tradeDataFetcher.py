import logging
import downloadManager as dm

"""
Uses UN Comm Trade Open API to fetch trade data of reporting country.
Free access to Open API has certain limitations, please refer to http://comtrade.un.org/data/doc/api/ for more info.
"""

open_api_url = "http://comtrade.un.org/api/get"
reporting_countries_url = "http://comtrade.un.org/data/cache/reporterAreas.json"
default_location = "/usr/local/data/commtrade/"
default_format = "csv"

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()


class Fetcher(object):

    def __init__(self):
        pass

    def read_and_save(self, fmt, r, ps, url, loc=default_location):
        """ fmt (csv), r (country code), ps (year) & url (comm trade url) """
        from urllib import request, parse, error
        # Dictionary of query parameters (if any)
        parms = {
            # Format as CSV
            'fmt': fmt,
            # Year (ps)
            'ps': ps,
            # Region (r). 156 is for China
            'r': r,
        }
        processed = False
        # Encode the query string
        querystring = parse.urlencode(parms)
        try:
            u = request.urlopen(url + '?' + querystring)
            f = open(loc + r + '-' + ps + '.' + fmt, 'wt')
            # Make a GET request and read the response

            resp = u.read()
            f.write(resp.decode('utf-8'))
            f.close()
            processed = True
        except error.HTTPError as e:
            logger.error("HTTP Error {}", e)
        return processed


    def get_data_for(self, manager, country, from_year, to_year, fmt=default_format):
        import time
        for i in range(from_year, to_year):
            logger.info('Processing for year - {}'.format(i))
            time.sleep(1)
            manager.send(self.read_and_save, fmt, country, str(i), open_api_url)

    def get_reporting_countries(self):
        """ Returns dict of country & id """
        from urllib import request
        import json
        u = request.urlopen(reporting_countries_url)
        response = u.read()
        reporters = json.loads(response.decode('utf-8'))
        values = reporters['results']
        countries = {}
        for s in values:
            countries[s['text']] = s['id']
        return countries

    def process(self):
        countries = self.get_reporting_countries()
        from_year = 1990
        to_year = 2016
        # Open API only supports 1000 requests per hour
        # Throttling
        manager = dm.DownloadManager()

        for k, v in countries.items():
            if k == 'All' or k == 'World' or v == '579' or v == '156' or v == '381':
                continue
            logger.info('Processing for {} id is {}'.format(k, v))
            self.get_data_for(manager, v, from_year, to_year)


if __name__ == '__main__':
    fetcher = Fetcher()
    fetcher.process()



