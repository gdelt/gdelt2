import quandl

try:
    import unicodecsv as csv
except ImportError:
    import warnings

    warnings.warn("can't import `unicodecsv` encoding errors may occur")

quandl.ApiConfig.api_key = 'H7Qi-N6FwL_BsW3YhCvp'
quandl.ApiConfig.api_version = '2015-04-09'

quandl_db_code = 'UCOM'


def get_commodity_keys_dict():
    return dict_from_csv('ProductName_Code_CommTrade.csv')


def get_country_code_dict():
    return dict_from_csv('Country_Codes_Quandl_CommTrade.csv')


def get_ucom_codes_dict():
    return dict_from_csv('UCOM-datasets-codes.csv')


def dict_from_csv(file_name):
    code_value_dct = {}
    with open(file_name) as dict_keys:
        reader = csv.reader(dict_keys)
        code_value_dct = {rows[0]: rows[1] for rows in reader}
    return code_value_dct


def query_quandl(code):
    q_qstr = quandl_db_code+'/'+code
    print "Querying Quandl for ",q_qstr
    mydata  =  quandl.get(q_qstr)
    print mydata


if __name__ == '__main__':
    cntry_codes = get_country_code_dict()
    cmmdty_codes = get_commodity_keys_dict()
    for key, value in cntry_codes.iteritems():
        print "Getting data for country %s" % key
        for commkey, commval in cmmdty_codes.iteritems():
            print "Getting data for commodity %s" % commkey
            q_code = commval + '_' + value
            query_quandl(q_code)

