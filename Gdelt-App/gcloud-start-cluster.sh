gcloud dataproc clusters create gdelt-cluster \
    --metadata "JUPYTER_PORT=8124" \
    --initialization-actions \
        gs://dataproc-initialization-actions/jupyter/jupyter.sh \
    --bucket gdeltbucket \
	--zone us-east1-b \
	--master-boot-disk-size 500 \
    --num-workers 2 \
    --properties spark:spark.jars.packages=com.databricks:spark-csv_2.11:1.2.0,spark:spark.executorEnv.PYTHONHASHSEED=0,spark:spark.yarn.am.memory=1024m \
    --worker-machine-type=n1-highmem-2 \
    --master-machine-type=n1-highmem-2 \
	--worker-boot-disk-size 500 \
	--project gdelt-1325
