import avro.schema
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from  gdeltrecord import read_gdelt_data
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__).setLevel(logging.DEBUG)

def parse_schema(path="avro-schema.avsc"):
    logger.info("Parsing avro schema ")
    with open(path, 'r') as schema:
        return avro.schema.parse(schema.read())


def serialize_records(records, outpath="/Users/satishterala/Work/gdelt/sample_data/198310.avro"):
    schema = parse_schema()
    logger.info("Will write the out file to path %s "%(outpath))
    with open(outpath, 'wb') as out:
        writer = DataFileWriter(out, DatumWriter(), schema)
        for record in records:
            record = dict((f, getattr(record, f)) for f in record._fields)
            record['SQLDATE'] = record['SQLDATE'].strftime('%Y-%m-%dT%H:M:S')
            writer.append(record)


if __name__ == '__main__':
    records = []
    for row in read_gdelt_data('/Users/satishterala/Work/gdelt/sample_data/198010.csv'):
        records.append(row)
    serialize_records(records)
