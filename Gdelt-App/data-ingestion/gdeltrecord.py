from datetime import datetime
from collections import namedtuple

try:
    import unicodecsv as csv
except ImportError:
    import warnings

    warnings.warn("can't import `unicodecsv` encoding errors may occur")


fields = (
    "GLOBALEVENTID", "SQLDATE", "MonthYear", "Year", "FractionDate", "Actor1Code", "Actor1Name", "Actor1CountryCode",
    "Actor1KnownGroupCode", "Actor1EthnicCode", "Actor1Religion1Code", "Actor1Religion2Code", "Actor1Type1Code",
    "Actor1Type2Code", "Actor1Type3Code", "Actor2Code", "Actor2Name", "Actor2CountryCode", "Actor2KnownGroupCode",
    "Actor2EthnicCode", "Actor2Religion1Code", "Actor2Religion2Code", "Actor2Type1Code", "Actor2Type2Code",
    "Actor2Type3Code", "IsRootEvent", "EventCode", "EventBaseCode", "EventRootCode", "QuadClass", "GoldsteinScale",
    "NumMentions", "NumSources", "NumArticles", "AvgTone", "Actor1Geo_Type", "Actor1Geo_FullName",
    "Actor1Geo_CountryCode",
    "Actor1Geo_ADM1Code", "Actor1Geo_Lat", "Actor1Geo_Long", "Actor1Geo_FeatureID", "Actor2Geo_Type",
    "Actor2Geo_FullName",
    "Actor2Geo_CountryCode", "Actor2Geo_ADM1Code", "Actor2Geo_Lat", "Actor2Geo_Long", "Actor2Geo_FeatureID",
    "ActionGeo_Type", "ActionGeo_FullName", "ActionGeo_CountryCode", "ActionGeo_ADM1Code", "ActionGeo_Lat",
    "ActionGeo_Long", "ActionGeo_FeatureID", "DATEADDED", "SOURCEURL")


class GdeltRecord(namedtuple('GdeltRecord_', fields)):
    @classmethod
    def parse(klass, row):
        row = list(row)  ## make row mutable
        row[1] = datetime.strptime(row[1], '%Y%m%d')

        ##QuadClass
        row[29] = klass.int_or_zero(row[29])
        ## Goldstein score
        row[30] = klass.float_or_zero(row[30])
        # NumMentions
        row[31] = klass.int_or_zero(row[31])
        # NumSources
        row[32] = klass.int_or_zero(row[32])
        # NumArticles
        row[33] = klass.int_or_zero(row[33])
        # AvgTone
        row[34] = klass.float_or_zero(row[34])
        # Actor1Geo_Type
        row[35] = klass.int_or_zero(row[35])
        # Actor1Geo_Lat
        row[39] = klass.float_or_zero(row[39])
        # Actor1Geo_Long
        row[40] = klass.float_or_zero(row[40])
        # Actor1Geo_FeatureID
        #row[41] = klass.int_or_zero(row[41])
        # Actor2Geo_Type
        row[42] = klass.int_or_zero(row[42])
        # Actor2Geo_Lat
        row[46] = klass.float_or_zero(row[46])
        # Actor2Geo_Long
        row[47] = klass.float_or_zero(row[47])
        # Actor2Geo_FeatureID
        #row[48] = klass.int_or_zero(row[48])
        row[49] = klass.int_or_zero(row[49])
        # ActionGeo_Lat
        row[53] = klass.float_or_zero(row[53])
        #    ActionGeo_Long
        row[54] = klass.float_or_zero(row[54])
        # ActionGeo_FeatureID
        #row[55] = klass.int_or_zero(row[55])
        row[56] = klass.int_or_zero(row[56])  # DATEADDED
        if (len(row) == 56):
            row.append("")
        return klass(*row)

    @classmethod
    def int_or_zero(self, str):
        if self.is_str_not_empty(str):  # DATEADDED
            return int(str)
        else:
            return 0

    @classmethod
    def float_or_zero(self, str):
        if self.is_str_not_empty(str):  # DATEADDED
            return float(str)
        else:
            return 0.0

    @staticmethod
    def is_str_not_empty(str):
        if len(str) > 0:
            return True
        return False


def read_gdelt_data(path):
    with open(path, 'rU') as data:
        reader = csv.reader(data, delimiter='\t')
        for row in map(GdeltRecord.parse, reader):
            yield row


if __name__ == '__main__':
    for row in read_gdelt_data('/Users/satishterala/Work/gdelt/sample_data/198010.csv'):
        print row
        print type(row)
        break
