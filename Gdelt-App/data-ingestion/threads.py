from threading import Thread
from dataInjestor import gdelt
class DownloadWorker(Thread):

   def __init__(self, queue):
       Thread.__init__(self)
       self.queue = queue

   def run(self):
       while True:
           # Get the work from the queue and expand the tuple
            link = self.queue.get()
            gdelt_downloader = gdelt()
            gdelt_downloader.rationalize_file_format(link)
            self.queue.task_done()
