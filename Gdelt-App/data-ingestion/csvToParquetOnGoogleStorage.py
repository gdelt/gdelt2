"""
Application to read the csv files for individual years from google storage location
and persist them back into parquet format using Spark.

"""

import argparse
import json
import os
from googleapiclient import discovery

from oauth2client.client import GoogleCredentials
from collections import namedtuple


def create_service():
    """Creates the service object for calling the Cloud Storage API."""
    # Get the application default credentials. When running locally, these are
    # available after running `gcloud init`. When running on compute
    # engine, these are available from the environment.
    credentials = GoogleCredentials.get_application_default()

    # Construct the service object for interacting with the Cloud Storage API -
    # the 'storage' service, at version 'v1'.
    # You can browse other available api services and versions here:
    #     https://developers.google.com/api-client-library/python/apis/
    return discovery.build('storage', 'v1', credentials=credentials)


def get_bucket_metadata(bucket):
    """Retrieves metadata about the given bucket."""
    service = create_service()

    # Make a request to buckets.get to retrieve a list of objects in the
    # specified bucket.
    req = service.buckets().get(bucket=bucket)
    return req.execute()


def list_bucket(bucket):
    """Returns a list of metadata of the objects within the given bucket."""
    service = create_service()

    # Create a request to objects.list to retrieve a list of objects.
    fields_to_return = \
        'nextPageToken,items(name,size,contentType,metadata(my-key))'
    req = service.objects().list(bucket=bucket, fields=fields_to_return)

    all_objects = []
    # If you have too many items to list in one request, list_next() will
    # automatically handle paging with the pageToken.
    while req:
        resp = req.execute()
        all_objects.extend(resp.get('items', []))
        req = service.objects().list_next(req, resp)
    return all_objects

def list_of_files(bucket):
    BucketContent = namedtuple('BucketContent',['contentType','name','size'])
    bucket_data= list_bucket(bucket)
    bucketContents = [BucketContent(**k) for k in bucket_data]
    return bucketContents

#csv_data/1987.csv
def construct_parquet_path(bucketContent):
    file_size = int(bucketContent.size)
    if(file_size > 0):
        file_name = bucketContent.name[9:]
        file_name_dttime  = os.path.splitext(file_name)[0]
        final_name = strip_export(os.path.splitext(file_name_dttime)[0])
        file_year =  final_name[0:4]
        return ("gs://gdeltbucket-data/csv_data/"+file_name,"gs://gdeltbucket-data/parquet_data/"+file_year+"/"+final_name+".parquet")

def strip_export(file_name_str):
    if file_name_str.find('.export')!=-1:
        return file_name_str[:file_name_str.find('.export')]
    else:
        return file_name_str


def write_parquet(input_path,output_path):
    df = sqlContext.read \
    .format('com.databricks.spark.csv') \
    .options(header='false') \
    .load(input_path, schema = customSchema)

    df.write.save(output_path,format="parquet")

def main(bucket):
    bucket_list = list_of_files(bucket)
    for bucket_item in bucket_list:
        path =  construct_parquet_path(bucketContent=bucket_item)
        if path is not None:
            write_parquet(path[0],path[1])

