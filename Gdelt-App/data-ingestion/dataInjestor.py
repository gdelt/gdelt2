import StringIO
import logging
import zipfile
from multiprocessing.pool import ThreadPool
from os import listdir
from os.path import isfile, join
from threading import Thread
from time import time

import avro.schema
import requests
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from bs4 import BeautifulSoup

import gdeltrecord

try:
    import unicodecsv as csv
except ImportError:
    import warnings

    warnings.warn("can't import `unicodecsv` encoding errors may occur")
    import csv

"""
GDelt object just stores
    url=http://data.gdeltproject.org/events/index.html
    base_url=http://data.gdeltproject.org/events
    base_dir=directory where the script is stored
    download_dir=basedir + "event_files"

    two lists:
    downloaded_files = extract the list of files existing in download_dir
    new_files = filenames extracted from url and not in downloaded_files

gdelt object on created will store urls and dirs
gdelt object should call get_downloaded_files first and then get_new_files
finally should call download_new_files
TODO: to verify the sizes before ignoring to add to newfiles list

"""

event_files_url = "http://data.gdeltproject.org/events/index.html"
intermediate_download_dir = "/usr/local/gdelt/intermediate/"
gcs_mount = "/mnt/gdeltbucket-data/"
#gcs_mount = "/usr/local/gdelt/avro_files/ "

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__).setLevel(logging.DEBUG)


class DownloadWorker(Thread):
    def __init__(self, queue, transform_q):
        Thread.__init__(self)
        self.queue = queue
        self.transform_q = transform_q

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            link = self.queue.get()
            gdelt_downloader = gdelt()
            gdelt_downloader.rationalize_file_format(link)
            self.queue.task_done()


class AvroTransfomer(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            filename, z = self.queue.get()
            gdelt_downloader = gdelt()
            gdelt_downloader.avroFileTransfomer(filename, z)
            self.queue.task_done()


class gdelt(object):
    def __init__(self):
        self.event_files_url = event_files_url
        self.download_dir = intermediate_download_dir
        self.logger = logging.getLogger(__name__)
        self.gcs_mount = gcs_mount
        self.result_list = []
        self.logger.info("Parsing avro schema ")
        with open("avro-schema.avsc", 'r') as schema:
            self.schema = avro.schema.parse(schema.read())

    def get_downloaded_files_list(self):
        """
        :return: List of files already downloaded
        """
        return [file for file in listdir(self.download_dir) if isfile(join(self.download_dir, file))]

    def find_files(self):
        """
        Opens a connection using beautiful soup to the data page.
        Download all hyperlinked zip file urls.
        Does intermeidate clean ups for md5sums, filesizes and the master reduced V2 as we are actually downloading individual files.
        """
        soup = BeautifulSoup(requests.get(self.event_files_url).text)
        hrefs = []
        for a in soup.find_all('a'):
            hrefs.append(a['href'])
        hrefs.remove('md5sums')
        hrefs.remove('filesizes')
        hrefs.remove('GDELT.MASTERREDUCEDV2.1979-2013.zip')
        hrefs.sort()
        return hrefs

    def rationalize_file_format(self, link):
        """
        :param link: A link the actual file url returned by beautiful soup
        :return :the collection of files that were downloaded and rationalized for the minor format mismatch
        This method downloads the zip, from the zip extracts each individual file, goes them over row by row
        and checks for the format (58 vs 57 fields). And fixes them for any missing fields.
        Output is written to a local directory as an individual csv file. And then the csv files are returned as list.
        """
        r = requests.get("http://data.gdeltproject.org/events/" + link)
        downloaded_files = self.get_downloaded_files_list()
        filename_z = []
        with zipfile.ZipFile(StringIO.StringIO(r.content)) as z:
            for filename in z.namelist():
                self.logger.info("Processing file : %s" % filename)
                if filename not in downloaded_files:
                    self.logger.info(
                        "File : %s not already in the list of downloaded files, so downloading it" % (filename))
                    with z.open(filename) as f:
                        self.logger.info("Opening intermediate file : %s " % (filename))
                        with open('/usr/local/gdelt/intermediate/' + filename, 'w') as csvoutput:
                            writer = csv.writer(csvoutput)
                            self.logger.info("Processing file name %s " % (filename))
                            reader = csv.reader(f, delimiter='\t')
                            for row in reader:
                                if (len(row) != 58):
                                    row.append('')
                                writer.writerow(row)
                            self.logger.debug("Completed writing intermediate files to :" + filename)
                    filename_z.append(filename)
                else:
                    self.logger.info("File %s is already downloaded folder, skipping it " % (filename))
        self.logger.info("Returning file names with count %d " % len(filename_z))
        return filename_z

    def avroFileTransfomer(self, filename):
        """
        Method to transform a file given its name (location is assumed to be known) to an avro format from a csv format.
        Converts the csv into a named tuple collection and then writes out the the avro format
        :param filename:
        :return:
        """
        self.logger.debug("Start Avro Transform for %s" % filename)
        with open('/usr/local/gdelt/intermediate/' + filename, 'rU') as csv_file:
            records = []
            self.logger.info("Running Avro Transformation for  file name %s " % (filename))
            reader = csv.reader(csv_file)
            row_count = 0
            for row in reader:
                try:
                    records.append(gdeltrecord.GdeltRecord.parse(row))
                except:
                    self.logger("Error processing row with event id %s in file %s" % (row[1], filename))

            out_file_name = self.serialize_records(records, outpath=self.gcs_mount + filename + ".avro")
            return out_file_name

    def parse_schema(self, path="avro-schema.avsc"):
        """
        Avro schema parseer
        :param path:
        :return:
        """
        self.logger.info("Parsing avro schema ")
        with open(path, 'r') as schema:
            return avro.schema.parse(schema.read())

    def serialize_records(self, records, outpath="/Users/satishterala/Work/gdelt/sample_data/198310.avro"):
        """
        Does the actual work of converting a list of records to avro and writing them out.
        :param records:
        :param outpath:
        :return:
        """
        #schema = self.parse_schema()
        self.logger.info("Will write the out file to path %s " % (outpath))
        with open(outpath, 'wb') as out:
            writer = DataFileWriter(out, DatumWriter(), self.schema)
            for record in records:
                record = dict((f, getattr(record, f)) for f in record._fields)
                record['SQLDATE'] = record['SQLDATE'].strftime('%Y-%m-%dT%H:M:S')
                writer.append(record)
        return outpath

    def avro_complete(self, x):
        print "Avro processing complete for ", x
        self.result_list.append(x)


if __name__ == '__main__':
    gdelt_rec = gdelt()
    links = gdelt_rec.find_files()
    ts = time()

    # download_pool = ThreadPool(processes=30)
    # download_results = download_pool.map(gdelt_rec.rationalize_file_format, links)
    # download_pool.close()
    # download_pool.join()
    # rationalized_files = list(itertools.chain.from_iterable(download_results))
    # rationalized_files =  gdelt_rec.get_downloaded_files_list()

    rationalized_files = gdelt_rec.get_downloaded_files_list()

    gdelt_rec.logger.info("Found %d file in the downloaded file list " % (len(rationalized_files)))

    avro_conv_pool = ThreadPool(20)

    avro_conv_pool.map(gdelt_rec.avroFileTransfomer, rationalized_files)
    # avro_conv_pool.map_async(gdelt_rec.avroFileTransfomer, rationalized_files,callback=gdelt_rec.avro_complete)
    # for file_name in rationalized_files:
    #    gdelt_rec.logger.info("Assigning file %s to avropool"%file_name)


    avro_conv_pool.close()
    avro_conv_pool.join()

    print('Took {}'.format(time() - ts))
